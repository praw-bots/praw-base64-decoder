import praw
import config
import os
import base64

def bot_login():
    # Optional: print "Logging in..."
    r = praw.Reddit(username = config.username,
            password = config.password,
            client_id = config.client_id,
            client_secret = config.client_secret,
            user_agent = "[user-agent]")
    # Optional: print "Logged in!"
    return r
def run_bot(r, comments_replied_to):
    # Optional: print "Obtaining Comments..."
    
    for comment in r.subreddit('[subreddit]').stream.comments():
        if "[command]" in comment.body and comment.id not in comments_replied_to and not comment.author == r.user.me():
		s = comment.body
		l = list(s)
		l[0:13] = []
		s = "".join(l)
		if s.strip():
		 while True:
		   try:
	    		base64coderaw = comment.body
	    		base64code = base64coderaw[base64coderaw.find('[search_symbol_start]')+1:base64coderaw.find("[search_symbol_end]")]
			base64decoded = base64.b64decode(base64code)
	    		base64decodedunicode = base64decoded.decode('utf-8', 'ignore')
	    		author = comment.author
            		# Optional: print "String with \"[command]\" found in Comment " + comment.id + " from User " + author.name
            		comment.reply("Here's your converted Base64 code: \" " + base64decodedunicode  + " \" - Base64 code decoded for /u/" + author.name + "\n\n^[footnote]")
            		# Optional: print "Replied to comment " + comment.id
            		comments_replied_to.append(comment.id)
            		with open ("comments_replied_to.txt", "a") as f:
                		f.write(comment.id + "\n")
			# Optional: print("Successful")
			break
		   except TypeError:
			# Optional: print("Error. Bad Base64")
			author = comment.author
			comment.reply("Your Base64 code couldn't be converted. I'm sorry :(.\n\n^[footnote]")
			# Optional: print "Replied to comment " + comment.id
                        comments_replied_to.append(comment.id)
                        with open ("comments_replied_to.txt", "a") as f:
                                f.write(comment.id + "\n")
			# Optional: print("Unsuccessful")
			break
	  	else:
			author = comment.author
			# Optional: print "String with \"[command]\" found in Comment " + comment.id + " from User " + author.name
	    		comment.reply("Your Base64 code couldn't be converted. I'm sorry :(.\n\n^[footnote]")
                        # Optional: print "Replied to comment " + comment.id
                        comments_replied_to.append(comment.id)
                        with open ("comments_replied_to.txt", "a") as f:
                                f.write(comment.id + "\n")
			# Optional: print("Unsuccessful")
def get_saved_comments():
    if not os.path.isfile("comments_replied_to.txt"):
        comments_replied_to = []
    else:
        with open("comments_replied_to.txt", "r") as f:
            comments_replied_to = f.read()
            comments_replied_to = comments_replied_to.split("\n")
    
    return comments_replied_to

r = bot_login()
comments_replied_to = get_saved_comments()
# Optional: print comments_replied_to
while True:
    run_bot(r, comments_replied_to)
